//
//  Utilities.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 05/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import Foundation

class Utilities {
    static func stringToData(string: String) -> Data {
        return string.data(using: String.Encoding.utf8)!
    }
    
    static func dataToString(data: Data) -> String {
        return String(data: data, encoding: .ascii)!
    }
    
    static func getDocumentApplicationSupportDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let directory = paths[0].appendingPathComponent("Application Support")
        
        if !Utilities.fileExists(atPath: directory.absoluteString) {
            do {
                try Utilities.createDirectory(at: directory)
            } catch {
                print("Failed to create application support directoryß")
            }
        }
        return directory
    }
    
    static func getLibraryApplicationSupportDirectory() -> URL {
        let paths = FileManager.default.urls(for: .libraryDirectory, in: .userDomainMask)
        let directory = paths[0].appendingPathComponent("Application Support")
        
        if !Utilities.fileExists(atPath: directory.absoluteString) {
            do {
                try Utilities.createDirectory(at: directory)
            } catch {
                print("Failed to create application support directoryß")
            }
        }
        return directory
    }
    
    static func createDirectory(at url: URL) throws {
        do {
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        } catch {
            throw error
        }
    }
    
    static func bundlePath() -> String {
        return Bundle.main.bundlePath
    }
    
    static func fileExists(atPath path: String) -> Bool {
        
        let fileManager = FileManager.default
        
        return fileManager.fileExists(atPath: path)
    }
}
