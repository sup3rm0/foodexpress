//
//  DataController.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 05/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//


import UIKit
import CoreData

class DataController: NSObject {
    
    static let sharedInstance = DataController()
    
    var managedObjectContext: NSManagedObjectContext
    
    private override init() {
        // This resource is the same name as your xcdatamodeld contained in your project.
        guard let modelURL = Bundle.main.url(forResource: "FoodExpress", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        let psc = NSPersistentStoreCoordinator(managedObjectModel: mom)
        self.managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        self.managedObjectContext.persistentStoreCoordinator = psc
        self.managedObjectContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        let docURL = Utilities.getLibraryApplicationSupportDirectory().appendingPathComponent("FoodExpress.sqlite")
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption: true,
                           NSInferMappingModelAutomaticallyOption: true]
            
            try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: docURL, options: options)
        } catch {
            print("Error migrating store: \(error)")
        }
    }
    
    
    func deleteEntityData(entityName: String, predicate: NSPredicate?) {
        //print("Delete entity = \(entityName), predicate = \(String(describing: predicate))")

        if #available(iOS 9.0, *) {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            if let p = predicate {
                fetchRequest.predicate = p
            }
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            
            do {
                _ = try self.managedObjectContext.execute(deleteRequest)
                do {
                    try self.managedObjectContext.save()
                }
                catch {
                    print("[DeleteData] Failure to save context: \(error.localizedDescription)")
                }
            }
            catch {
                print("[DeleteData] Failure to delete entity \(entityName): \(error.localizedDescription)")
            }
        }
    }
}
