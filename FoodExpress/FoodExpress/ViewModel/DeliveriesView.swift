//
//  DeliveriesView.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 05/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class DeliveriesView: UIView {

    public var tableView: UITableView!
    public var retryButton: UIButton!
    public var activityIndicatorView: NVActivityIndicatorView!
    public var statusLabel: UILabel!
    public let refreshControl = UIRefreshControl()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    func setupUI() {

        // Table View
        tableView = UITableView(frame: CGRect.zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.isHidden = true
        tableView.register(DeliveryTableViewCell.classForCoder(), forCellReuseIdentifier: "deliveryTableViewCellIdentifier")
        addSubview(tableView)
        
        addConstraints([
            NSLayoutConstraint(item: tableView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .height, relatedBy: .equal, toItem: self, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: 0),
            ])
        
        // Add Refresh Control to Table View
        refreshControl.tintColor = UIColor(red: 243.0/255.0, green: 105.0/255.0, blue: 13.0/255.0, alpha: 1.0)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        
        
        // Activity Indicator
        activityIndicatorView = NVActivityIndicatorView(frame: CGRect.zero,
                                                        type: .ballTrianglePath ,
                                                        color: UIColor(red: 243.0/255.0, green: 105.0/255.0, blue: 13.0/255.0, alpha: 1.0),
                                                        padding: 0)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(activityIndicatorView)
        addConstraints([
            NSLayoutConstraint(item: activityIndicatorView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 50),
            NSLayoutConstraint(item: activityIndicatorView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 50),
            NSLayoutConstraint(item: activityIndicatorView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: activityIndicatorView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: -50),
            ])

        // Retry button
        retryButton = UIButton(type: .custom)
        retryButton.setImage(UIImage(named: "reload_40x40"), for: .normal)
        retryButton.translatesAutoresizingMaskIntoConstraints = false
        addSubview(retryButton)
        addConstraints([
            NSLayoutConstraint(item: retryButton, attribute: .width, relatedBy: .equal, toItem: activityIndicatorView, attribute: .width, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: retryButton, attribute: .height, relatedBy: .equal, toItem: activityIndicatorView, attribute: .height, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: retryButton, attribute: .centerX, relatedBy: .equal, toItem: activityIndicatorView, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: retryButton, attribute: .centerY, relatedBy: .equal, toItem: activityIndicatorView, attribute: .centerY, multiplier: 1.0, constant: 0),
            ])
        
        // Status Label
        statusLabel = UILabel(frame: CGRect.zero)
        statusLabel.translatesAutoresizingMaskIntoConstraints = false
        statusLabel.textAlignment = .center
        statusLabel.textColor = .darkGray
//        statusLabel.backgroundColor = .green
        statusLabel.numberOfLines = 0
        statusLabel.font = UIFont(name: "Lato-Regular", size: 14.0)
        addSubview(statusLabel)
        addConstraints([
            NSLayoutConstraint(item: statusLabel, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: -50),
            NSLayoutConstraint(item: statusLabel, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .height, multiplier: 1.0, constant: 30),
            NSLayoutConstraint(item: statusLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: statusLabel, attribute: .bottom, relatedBy: .equal, toItem: activityIndicatorView, attribute: .bottomMargin, multiplier: 1.0, constant: 50),
         ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
