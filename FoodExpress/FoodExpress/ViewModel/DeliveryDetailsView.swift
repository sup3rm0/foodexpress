//
//  DeliveryDetailsView.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 07/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit
import MapKit

class DeliveryDetailsView: UIView {

    public var mapView: MKMapView!
    public var headerView: UIView!
    private var thumbnailBorderView: UIView!
    public var thumbnailImage:UIImageView!
    public var descriptionTextView:UITextView!
    public var moreButton: UIButton!
    public var headerViewHeightConstraint: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .white
        
        mapView = MKMapView(frame: CGRect.zero)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(mapView)
        addConstraints([
            NSLayoutConstraint(item: mapView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 84),
            NSLayoutConstraint(item: mapView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: mapView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0),
            ])
        
        headerView = UIView(frame: CGRect.zero)
        headerView.translatesAutoresizingMaskIntoConstraints = false
        headerView.backgroundColor = UIColor(red: 250.0/255.0, green: 250.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        addSubview(headerView)
        
        headerViewHeightConstraint = NSLayoutConstraint(item: headerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 84)
        
        
        addConstraints([
            NSLayoutConstraint(item: headerView, attribute: .width, relatedBy: .equal, toItem: self, attribute: .width, multiplier: 1.0, constant: 0),
            headerViewHeightConstraint,
            NSLayoutConstraint(item: headerView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: headerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0),
            ])
        
        let underlineView = UIView(frame: CGRect.zero)
        underlineView.translatesAutoresizingMaskIntoConstraints = false
        underlineView.backgroundColor = UIColor(red: 215.0/255.0, green: 215.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        headerView.addSubview(underlineView)
        addConstraints([
            NSLayoutConstraint(item: underlineView, attribute: .width, relatedBy: .equal, toItem: headerView, attribute: .width, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: underlineView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 1),
            NSLayoutConstraint(item: underlineView, attribute: .centerX, relatedBy: .equal, toItem: headerView, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: underlineView, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            ])
        
        
        thumbnailBorderView = UIView(frame: CGRect.zero)
        thumbnailBorderView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailBorderView.backgroundColor = .clear
        thumbnailBorderView.layer.borderWidth = 1
        thumbnailBorderView.layer.borderColor = UIColor(red: 243.0/255.0, green: 105.0/255.0, blue: 13.0/255.0, alpha: 1.0).cgColor
        thumbnailBorderView.layer.cornerRadius = 30.0
        headerView.addSubview(thumbnailBorderView)
        
        addConstraints([
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 60),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 60),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .left, relatedBy: .equal, toItem: headerView, attribute: .left, multiplier: 1.0, constant: 15),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1.0, constant: 10),
            ])
        
        
        thumbnailImage = UIImageView(frame: CGRect.zero)
        thumbnailImage.contentMode = .scaleAspectFill
        thumbnailImage.layer.masksToBounds = true
        thumbnailImage.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImage.backgroundColor = UIColor(red: 241.0/255.0, green: 242.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        thumbnailImage.layer.cornerRadius = 25.0
        thumbnailBorderView.addSubview(thumbnailImage)
        
        addConstraints([
            NSLayoutConstraint(item: thumbnailImage, attribute: .width, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .width, multiplier: 1.0, constant: -10),
            NSLayoutConstraint(item: thumbnailImage, attribute: .height, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .height, multiplier: 1.0, constant: -10),
            NSLayoutConstraint(item: thumbnailImage, attribute: .centerX, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: thumbnailImage, attribute: .centerY, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .centerY, multiplier: 1.0, constant: 0),
            ])
        
        descriptionTextView = UITextView(frame: CGRect.zero)
        descriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        descriptionTextView.textAlignment = .left
        descriptionTextView.font = UIFont(name: "Lato-Regular", size: 14.0)
        descriptionTextView.isScrollEnabled =  false
        descriptionTextView.allowsEditingTextAttributes = false
        descriptionTextView.isEditable = false
        descriptionTextView.backgroundColor = .clear
        headerView.addSubview(descriptionTextView)
        
        addConstraints([
            NSLayoutConstraint(item: descriptionTextView, attribute: .trailing, relatedBy: .equal, toItem: headerView, attribute: .trailing, multiplier: 1.0, constant: -20),
            NSLayoutConstraint(item: descriptionTextView, attribute: .leading, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .trailing, multiplier: 1.0, constant: 10),
            NSLayoutConstraint(item: descriptionTextView, attribute: .top, relatedBy: .equal, toItem: headerView, attribute: .top, multiplier: 1.0, constant: 5),
            NSLayoutConstraint(item: descriptionTextView, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            ])
        
        
        moreButton = UIButton(type: .custom)
        moreButton.translatesAutoresizingMaskIntoConstraints = false
        moreButton.setImage(UIImage(named: "more_15x15"), for: .normal)
        moreButton.backgroundColor = .clear
        headerView.addSubview(moreButton)
        
        addConstraints([
            NSLayoutConstraint(item: moreButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 60),
            NSLayoutConstraint(item: moreButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20),
            NSLayoutConstraint(item: moreButton, attribute: .centerX, relatedBy: .equal, toItem: headerView, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: moreButton, attribute: .bottom, relatedBy: .equal, toItem: headerView, attribute: .bottom, multiplier: 1.0, constant: 0),
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
