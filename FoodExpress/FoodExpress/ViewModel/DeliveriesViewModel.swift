//
//  DeliveriesViewModel.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 05/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import Foundation
import CoreData

typealias DownloadComplete = (_ success: Bool, _ error: String?) -> Void
typealias FetchComplete = () -> Void

class DeliveriesViewModel: NSObject {
    
    private let apiClient = APIClient()
    private var isUpdating: Bool = false
    public var deliveries = [Delivery]()
    
    
    func getDeliveries(completionHandler: @escaping DownloadComplete) {
        
        if self.isUpdating {
            return
        }
        
        self.isUpdating = true
        self.apiClient.downloadDeliveries( completionHandler:{ (result: Any?, error: String?) in
            
            if error != nil {
                print("Error: \(error!)")
                self.isUpdating = false
                completionHandler(false, error)
                return
            }
            
            let dataController = DataController.sharedInstance
            let managedObjectCtx = dataController.managedObjectContext
            dataController.deleteEntityData(entityName: "Delivery", predicate: nil)
            
            // Save the data in core data for offline access
            if let items = result as? Array<[String:Any]> {
                for item in items {
                    
//                    print("item = \(item)")
                    
                    let delivery = NSEntityDescription.insertNewObject(forEntityName: "Delivery", into: managedObjectCtx) as! Delivery
                    
                    if let value = item["description"] as? String {
                        delivery.details = value
//                        delivery.details = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tristique sit amet nulla vel accumsan. Sed sed ornare ligula, non molestie odio."
                    }
                    
                    if let value = item["imageUrl"] as? String {
                        delivery.imageUrl = value
                    }
                    
                    if let location = item["location"] as? [String:Any] {
                        if let value = location["lat"] as? Double {
                            delivery.latitude = value
                        }
                        if let value = location["lng"] as? Double {
                            delivery.longitude = value
                        }
                        if let value = location["address"] as? String {
                            delivery.address = value
                        }
                    }
                    
                    do {
                        try managedObjectCtx.save()
                    }
                    catch {
                        print("Failure to save context: \(error)")
                    }
                }
            }
            
            self.isUpdating = false
            self.fetchData(completionHandler: {
                completionHandler(true, nil)
            })
        })
    }
    
    func fetchData(completionHandler: @escaping FetchComplete) {
        let dataController = DataController.sharedInstance
        let managedObjectCtx = dataController.managedObjectContext
        
        let request: NSFetchRequest<Delivery> = Delivery.fetchRequest()
        self.deliveries.removeAll()
        do {
            let result = try managedObjectCtx.fetch(request as! NSFetchRequest<NSFetchRequestResult>) as! [Delivery]
            self.deliveries = result
        }
        catch {
            print("Error \(error.localizedDescription) ")
        }        
        completionHandler()
    }
    
    func numberOfItems(in section: Int) -> Int {
        return self.deliveries.count
    }
}
