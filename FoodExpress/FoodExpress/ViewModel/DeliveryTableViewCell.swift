//
//  DeliveryTableViewCell.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 06/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit
import SDWebImage

class DeliveryTableViewCell: UITableViewCell {

    private var thumbnailBorderView: UIView!
    public var thumbnailImage:UIImageView!
    public var descriptionLabel:UILabel!
    public var addressLabel:UILabel!
    
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        accessoryType = .disclosureIndicator
        
        
        thumbnailBorderView = UIView(frame: CGRect.zero)
        thumbnailBorderView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailBorderView.backgroundColor = .clear
        thumbnailBorderView.layer.borderWidth = 1
        thumbnailBorderView.layer.borderColor = UIColor(red: 243.0/255.0, green: 105.0/255.0, blue: 13.0/255.0, alpha: 1.0).cgColor
        thumbnailBorderView.layer.cornerRadius = 30.0
        contentView.addSubview(thumbnailBorderView)
        
        addConstraints([
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 60),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 60),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .left, relatedBy: .equal, toItem: contentView, attribute: .left, multiplier: 1.0, constant: 15),
            NSLayoutConstraint(item: thumbnailBorderView, attribute: .centerY, relatedBy: .equal, toItem: contentView, attribute: .centerY, multiplier: 1.0, constant: 0),
            ])
        
        
        thumbnailImage = UIImageView(frame: CGRect.zero)
        thumbnailImage.contentMode = .scaleAspectFill
        thumbnailImage.layer.masksToBounds = true
        thumbnailImage.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImage.backgroundColor = UIColor(red: 241.0/255.0, green: 242.0/255.0, blue: 244.0/255.0, alpha: 1.0)
        thumbnailImage.layer.cornerRadius = 25.0
        thumbnailBorderView.addSubview(thumbnailImage)
        
        addConstraints([
            NSLayoutConstraint(item: thumbnailImage, attribute: .width, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .width, multiplier: 1.0, constant: -10),
            NSLayoutConstraint(item: thumbnailImage, attribute: .height, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .height, multiplier: 1.0, constant: -10),
            NSLayoutConstraint(item: thumbnailImage, attribute: .centerX, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: thumbnailImage, attribute: .centerY, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .centerY, multiplier: 1.0, constant: 0),
            ])
        
        descriptionLabel = UILabel(frame: CGRect.zero)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        descriptionLabel.textAlignment = .left
        descriptionLabel.font = UIFont(name: "Lato-Regular", size: 15.0)
        descriptionLabel.numberOfLines = 2
        descriptionLabel.lineBreakMode = .byTruncatingTail
        contentView.addSubview(descriptionLabel)
        
        addConstraints([
            NSLayoutConstraint(item: descriptionLabel, attribute: .trailing, relatedBy: .equal, toItem: contentView, attribute: .trailing, multiplier: 1.0, constant: -20),
            NSLayoutConstraint(item: descriptionLabel, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 30),
            NSLayoutConstraint(item: descriptionLabel, attribute: .leading, relatedBy: .equal, toItem: thumbnailBorderView, attribute: .trailing, multiplier: 1.0, constant: 10),
            NSLayoutConstraint(item: descriptionLabel, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1.0, constant: 10),
            ])
        
        
        addressLabel = UILabel(frame: CGRect.zero)
        addressLabel.translatesAutoresizingMaskIntoConstraints = false
        addressLabel.textAlignment = .left
        addressLabel.font = UIFont(name: "Lato-Regular", size: 13.0)
        addressLabel.textColor = .darkGray
        contentView.addSubview(addressLabel)
        
        addConstraints([
            NSLayoutConstraint(item: addressLabel, attribute: .width, relatedBy: .equal, toItem: descriptionLabel, attribute: .width, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: addressLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 20),
            NSLayoutConstraint(item: addressLabel, attribute: .centerX, relatedBy: .equal, toItem: descriptionLabel, attribute: .centerX, multiplier: 1.0, constant: 0),
            NSLayoutConstraint(item: addressLabel, attribute: .top, relatedBy: .equal, toItem: descriptionLabel, attribute: .bottom, multiplier: 1.0, constant: 0),
            ])
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
