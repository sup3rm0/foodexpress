//
//  DeliveriesViewController.swift
//  FoodExpress
//
//  Created by Hermoso Cariaga on 04/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit
import AMScrollingNavbar

class DeliveriesViewController: UIViewController {

    private var mainView: DeliveriesView { return view as! DeliveriesView }
    private let deliveriesViewModel = DeliveriesViewModel()
    
    override func loadView() {
        view = DeliveriesView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.tableView.delegate = self
        self.mainView.tableView.dataSource = self
        self.mainView.retryButton.addTarget(self, action: #selector(downloadData(_:)), for: .touchUpInside)
        
        self.mainView.refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        self.loadData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "My Deliveries"
        
        if let navigationController = self.navigationController as? ScrollingNavigationController {
            navigationController.followScrollView(self.mainView.tableView, delay: 50.0)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = navigationController as? ScrollingNavigationController {
            navigationController.stopFollowingScrollView()
        }
        self.title = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        self.mainView.activityIndicatorView.startAnimating()
        self.mainView.statusLabel.isHidden = false
        self.mainView.statusLabel.text = "Loading..."
        self.mainView.tableView.isHidden = true
        self.mainView.retryButton.isHidden = true
        
        self.deliveriesViewModel.fetchData(completionHandler: {
            if self.deliveriesViewModel.deliveries.count > 0 {
                DispatchQueue.main.async {
                    self.mainView.activityIndicatorView.stopAnimating()
                    self.mainView.statusLabel.isHidden = true
                    self.mainView.tableView.isHidden = false
                    self.mainView.tableView.reloadData()
                }
            }
            else {
                self.deliveriesViewModel.getDeliveries(completionHandler:{ (success: Bool, error: String?) in
                    DispatchQueue.main.async {
                        self.mainView.activityIndicatorView.stopAnimating()
                        if success {
                            self.mainView.statusLabel.isHidden = true
                            self.mainView.tableView.isHidden = false
                            self.mainView.tableView.reloadData()
                        }
                        else {
                            self.mainView.statusLabel.text = error
                            self.mainView.retryButton.isHidden = false
                        }
                    }
                })
            }
        })
    }
    
    
    
    @objc func downloadData(_ sender: UIButton) {
        self.loadData()
    }
    
    @objc func refreshData(_ sender: UIRefreshControl) {
        self.deliveriesViewModel.getDeliveries(completionHandler:{ (success: Bool, error: String?) in
            DispatchQueue.main.async {
                self.mainView.refreshControl.endRefreshing()
                self.mainView.tableView.reloadData()
            }
        })
    }
}

extension DeliveriesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "deliveryTableViewCellIdentifier", for: indexPath) as? DeliveryTableViewCell else {
            fatalError("failed to load 'DeliveryTableViewCell' with identifier 'deliveryTableViewCellIdentifier'")
        }
        
        let delivery = self.deliveriesViewModel.deliveries[indexPath.row]
        if let url = delivery.imageUrl {
            cell.thumbnailImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder_70x70"))
        }
        else {
            cell.thumbnailImage.image = UIImage(named: "placeholder_70x70")
        }
        
        cell.descriptionLabel.text = delivery.details!
        cell.addressLabel.text = delivery.address!
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.deliveriesViewModel.numberOfItems(in: section)
    }
}

extension DeliveriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 84.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let delivery = self.deliveriesViewModel.deliveries[indexPath.row]
        let deliveryDetails = DeliveryDetailsViewController()
        deliveryDetails.delivery = delivery
        self.navigationController?.pushViewController(deliveryDetails, animated: true)
    }
    
}

