//
//  APIClient.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 05/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit

typealias CompletionHandler = (_ result: Any?, _ error: String?) -> Void

class APIClient: NSObject {

    func downloadDeliveries(completionHandler: @escaping CompletionHandler) {
        
        let endpoint: String = "\(hostURL)/deliveries"
        guard let url = URL(string: endpoint) else {
            print("Failed to create URL")
            completionHandler(nil, "Failed to create URL")
            return
        }
        
        let urlRequest = URLRequest(url: url)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: urlRequest as URLRequest) { (data:Data?, response:URLResponse?, error:Error?) -> Void in
            
            guard error == nil else {
                print("Error: \(String(describing: error?.localizedDescription))")
                
                completionHandler(nil, error!.localizedDescription)
                
                return
            }
            
            guard let responseData = data else {
                print("Error: did not receive response data")
                completionHandler(nil, "Did not receive response data")
                return
            }
            
            do {
                guard let jsonResult = try JSONSerialization.jsonObject(with: responseData, options: []) as? Array<Any> else {
                    print("Guard: Failed to convert data to JSON")
                    completionHandler(nil, "Failed to convert data to JSON")
                    return
                }
                
                completionHandler(jsonResult, nil)
            } catch  {
                completionHandler(nil, "Failed to convert data to JSON")
                return
            }
        }
        dataTask.resume()
    }
}
