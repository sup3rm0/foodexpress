//
//  DeliveryDetailsViewController.swift
//  FoodExpress
//
//  Created by Mo Cariaga on 07/07/2018.
//  Copyright © 2018 Hermoso Cariaga. All rights reserved.
//

import UIKit
import MapKit

class DeliveryDetailsViewController: UIViewController, MKMapViewDelegate {

    var delivery: Delivery!
    private var mainView: DeliveryDetailsView { return view as! DeliveryDetailsView }
    
    override func loadView() {
        view = DeliveryDetailsView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Delivery Details"
        self.navigationController?.navigationBar.tintColor = UIColor(red: 243.0/255.0, green: 105.0/255.0, blue: 13.0/255.0, alpha: 1.0)
     
        if let url = delivery.imageUrl {
            self.mainView.thumbnailImage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placeholder_70x70"))
        }
        else {
            self.mainView.thumbnailImage.image = UIImage(named: "placeholder_70x70")
        }
        self.mainView.descriptionTextView.text = self.delivery.details!
        self.mainView.moreButton.addTarget(self, action: #selector(showMore(_:)), for: .touchUpInside)
        
        self.mainView.mapView.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: self.delivery.latitude, longitude: self.delivery.longitude)
        annotation.title = ""
        annotation.subtitle = self.delivery.address
        self.mainView.mapView.addAnnotation(annotation)
        
        let center = CLLocationCoordinate2D(latitude: self.delivery.latitude, longitude: self.delivery.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        self.mainView.mapView.setRegion(region, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func showMore(_ sender: UIButton) {
        if self.mainView.headerViewHeightConstraint.constant == 84 {
            self.mainView.moreButton.setImage(UIImage(named: "less_15x15"), for: .normal)
            UIView.animate(withDuration: 0.5, animations: {
                self.mainView.headerViewHeightConstraint.constant = 384.0
                
            }, completion: { (finished: Bool) in
                self.mainView.headerView.layoutIfNeeded()
            })
            self.mainView.descriptionTextView.isScrollEnabled = true
        }
        else {
            self.mainView.moreButton.setImage(UIImage(named: "more_15x15"), for: .normal)
            UIView.animate(withDuration: 0.5, animations: {
                self.mainView.headerViewHeightConstraint.constant = 84.0
            }, completion:  { (finished: Bool) in
                self.mainView.headerView.layoutIfNeeded()
            })
            self.mainView.descriptionTextView.isScrollEnabled = false
        }
    }
    
    // MARK: - MKMapView Delegate
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation { return nil }

        let identifier = "mapViewAnnotation"
        var annotationView = self.mainView.mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.image = UIImage(named: "mapAnnotation")!
        } else {
            annotationView!.annotation = annotation
        }
        return annotationView
    }
}
